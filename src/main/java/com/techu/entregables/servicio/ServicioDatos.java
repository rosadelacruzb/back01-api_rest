package com.techu.entregables.servicio;

import com.techu.entregables.modelo.ModeloProducto;
import com.techu.entregables.modelo.ModeloUsuario;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

@Component
public class ServicioDatos {
    private final AtomicInteger secuenciaIdsProductos = new AtomicInteger(0);
    private final AtomicInteger secuenciaIdsUsuarios = new AtomicInteger(0);
    private final List<ModeloProducto> productos = new ArrayList<ModeloProducto>();

    public ModeloProducto crearProducto(ModeloProducto producto) {
        producto.setId_Producto(this.secuenciaIdsProductos.incrementAndGet());
        this.productos.add(producto);
        return producto;
    }

    public List<ModeloProducto> obtenerProductos() {
        return Collections.unmodifiableList((this.productos));
    }

    public ModeloProducto obtenerProductoPorId(int id) {
        for (ModeloProducto p : this.productos) {
            if (p.getId_Producto() == id)
                return p;
        }
        return null;
    }

    public boolean actualizarProducto(int idProducto, ModeloProducto producto) {
        final ModeloProducto p =this.obtenerProductoPorId(idProducto);
        if (p == null)
            return false;
        p.setMarca(producto.getMarca());
        p.setDescripcion(producto.getDescripcion());
        p.setPrecio(producto.getPrecio());
        return true;
    }

    public boolean borrarProducto (int id){
        final ModeloProducto p =this.obtenerProductoPorId(id);
        if (p == null)
            return false;
        for(int i=0;i<this.productos.size();++i){
            if (this.productos.get(i).getId_Producto()==id){
                this.productos.remove(id-1);
                return true;
            }
        }
        return false;
    }

    public List<ModeloUsuario> obtenerUsuariosProducto(int idProducto){
        return Collections.unmodifiableList((this.obtenerProductoPorId(idProducto).getUsuarios()));
    }

    public ModeloUsuario agregarUsuarioProducto(int idProducto, ModeloUsuario usuario) {
        final ModeloProducto p =this.obtenerProductoPorId(idProducto);
        if (p == null)
            return null;
        usuario.setId_Usuario(this.secuenciaIdsUsuarios.incrementAndGet());
        usuario.setNombre(usuario.getNombre());
        p.getUsuarios().add(usuario);
        return usuario;
    }

    public ModeloUsuario obtenerUsuarioProducto(int idProducto, int idUsuario){
        final ModeloProducto p =this.obtenerProductoPorId(idProducto);
        if (p == null)
            return null;
        for(ModeloUsuario u: p.getUsuarios()){
            if(u.getId_Usuario() == idUsuario){
                return u;
            }
        }
        return null;
    }

    public boolean actualizarUsuarioProducto(int idProducto, int idUsuario, ModeloUsuario usuario){
        List<ModeloUsuario> usuarios = this.obtenerProductoPorId(idProducto).getUsuarios();
        for (int i = 0; i < usuarios.size();++i){
            if(usuarios.get(i).getId_Usuario() == idUsuario){
                usuario.setId_Usuario(idUsuario);
                usuarios.set(i,usuario);
                return true;
            }
        }
        return false;
    }

    public boolean borrarUsuarioProducto (int idProducto, int IdUsuario){
        List<ModeloUsuario> usuarios = this.obtenerProductoPorId(idProducto).getUsuarios();
        for(int i=0; i < usuarios.size();++i) {
            if (usuarios.get(i).getId_Usuario() == IdUsuario) {
                usuarios.remove(i);
                return true;
            }
        }
        return false;
    }
}