package com.techu.entregables.modelo;

public class ModeloUsuario {
    private int id_Usuario;
    private String nombre;

    public ModeloUsuario(int id_Usuario, String nombre) {
        this.id_Usuario = id_Usuario;
        this.nombre = nombre;
    }

    public int getId_Usuario() {
        return id_Usuario;
    }

    public void setId_Usuario(int id_Usuario) {
        this.id_Usuario = id_Usuario;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public String toString() {
        return "ModeloUsuario{" +
                "id=" + id_Usuario +
                ", nombre='" + nombre + '\'' +
                '}';
    }
}
