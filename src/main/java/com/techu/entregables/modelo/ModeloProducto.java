package com.techu.entregables.modelo;

import com.sun.el.lang.ELArithmetic;

import java.util.ArrayList;
import java.util.List;

public class ModeloProducto {
    private int id_Producto;
    private String marca;
    private String descripcion;
    private Double precio;
    private List<ModeloUsuario> usuarios;

//public ModeloProducto(int id_Producto, String marca, String descripcion, Double precio, List<ModeloUsuario> usuarios) {
    public ModeloProducto(int id_Producto, String marca, String descripcion, Double precio) {
        this.id_Producto = id_Producto;
        this.marca = marca;
        this.descripcion = descripcion;
        this.precio = precio;
//        this.usuarios = usuarios;
        this.usuarios = new ArrayList<ModeloUsuario>();
    }

    public int getId_Producto() {
        return id_Producto;
    }

    public void setId_Producto(int id_Producto) {
        this.id_Producto = id_Producto;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Double getPrecio() {
        return precio;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }

    public List<ModeloUsuario> getUsuarios() {
        return usuarios;
    }

//    public void setUsuarios(List<ModeloUsuario> usuarios) {
//        this.usuarios = usuarios;
//    }
}
