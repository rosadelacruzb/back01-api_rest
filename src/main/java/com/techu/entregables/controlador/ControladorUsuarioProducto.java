package com.techu.entregables.controlador;

import com.techu.entregables.modelo.ModeloProducto;
import com.techu.entregables.modelo.ModeloUsuario;
import com.techu.entregables.servicio.ServicioDatos;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/v1/productos")
public class ControladorUsuarioProducto {

        @Autowired
        private ServicioDatos servicioDatos;

        @PostMapping("/{productId}/usuarios")
        public ResponseEntity crearUsuarioProducto(@PathVariable int productId,
                                                   @RequestBody ModeloUsuario usuario){
            final ModeloUsuario u = this.servicioDatos.agregarUsuarioProducto(productId, usuario);
            return (u == null)
                    ? new ResponseEntity<>(HttpStatus.NOT_FOUND)
                    :ResponseEntity.ok(u);
        }

        @GetMapping("/{productId}/usuarios/{usuarioId}")
        public ResponseEntity obtenerUsuarioDeUnProducto(@PathVariable int productId,
                                                         @PathVariable int usuarioId){
            final ModeloUsuario u = this.servicioDatos.obtenerUsuarioProducto(productId,usuarioId);
            return (u == null)
                ? new ResponseEntity<>(HttpStatus.NOT_FOUND)
                :ResponseEntity.ok(u);
        }

        @GetMapping("/{productId}/usuarios")
        public ResponseEntity obtenerUsuariosDeUnProducto(@PathVariable int productId){
            return (this.servicioDatos.obtenerUsuariosProducto(productId) == null)
                    ? new ResponseEntity<>(HttpStatus.NOT_FOUND)
                    :ResponseEntity.ok(this.servicioDatos.obtenerUsuariosProducto(productId));
        }
    }