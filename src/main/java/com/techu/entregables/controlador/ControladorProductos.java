package com.techu.entregables.controlador;
import com.techu.entregables.modelo.ModeloProducto;
import com.techu.entregables.modelo.ModeloUsuario;
import com.techu.entregables.servicio.ServicioDatos;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/productos")
public class ControladorProductos {

    @Autowired
    private ServicioDatos servicioDatos;

    @PostMapping("/")
    public ResponseEntity crearProducto(@RequestBody ModeloProducto producto){
        final ModeloProducto p = this.servicioDatos.crearProducto(producto);
        return ResponseEntity.ok(p);
    }

    @GetMapping("/")
    public ResponseEntity obtenerProductos(){
        return ResponseEntity.ok(this.servicioDatos.obtenerProductos());
    }

    @GetMapping("/{productId}")
    public ResponseEntity obtenerUnProducto(@PathVariable int productId){
        final ModeloProducto p =this.servicioDatos.obtenerProductoPorId(productId);
        return (p == null)
                ? new ResponseEntity<>(HttpStatus.NOT_FOUND)
                : ResponseEntity.ok(p);
    }

    @PutMapping("/{productId}")
    public ResponseEntity actualizarProducto(@PathVariable int productId,
                                             @RequestBody ModeloProducto productoUpd) {
        return (this.servicioDatos.actualizarProducto(productId, productoUpd))
                ? ResponseEntity.ok(productoUpd)
                :new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @PatchMapping("/{productId}")
    public ResponseEntity actualizarPrecioProducto(@PathVariable int productId,
                                            @RequestBody ModeloProducto productoPrecioOnly){
        final ModeloProducto p = this.servicioDatos.obtenerProductoPorId(productId);
        if (p == null)
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        p.setPrecio(productoPrecioOnly.getPrecio());
        return (this.servicioDatos.actualizarProducto(productId, p))
                ? ResponseEntity.ok(p)
                :new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @DeleteMapping("/{productId}")
    public ResponseEntity borrarProducto(@PathVariable int productId) {
        return (this.servicioDatos.borrarProducto(productId))
                ? new ResponseEntity<>("", HttpStatus.OK)
                :new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}